import express from 'express'
import cors from 'cors'
import { Configuration, OpenAIApi } from 'openai'
import * as dotenv from 'dotenv'
//express-ws is almost impossible to "import" ? wtf?
import { createRequire } from 'module';
const require = createRequire(import.meta.url);
const expressWsPackage = require('express-ws');


// Load environment variables from .env file
try {
  dotenv.config()
} catch (error) {
  console.error('Error loading environment variables:', error)
  process.exit(1)
}

// Create OpenAI configuration
const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
})

// Create OpenAI API client
const openai = new OpenAIApi(configuration)
const app = express()  // Create Express app
app.use(express.json()) // Parse JSON in request body
app.use(cors()) // Enable CORS
expressWsPackage(app);





app.ws('/davinci', async (ws, req) => {
  ws.on('message', async (message) => {
    const { prompt, parameters } = JSON.parse(message);
   
    const {temperature = 0.5, max_tokens = 50, top_p = 0.5, frequency_penalty = 0, presence_penalty = 0} = parameters;
    console.log(parameters)

    try {
      const res = await openai.createChatCompletion({
        model: 'gpt-4-1106-preview',  //gpt-3.5-turbo, gpt-4-1106-vision-preview, 
        messages: [
          { "role": "assistant", "content": "Hi! How can I help you?" },
          { "role": "user", "content": `${prompt}?` }
        ],
        temperature,
        max_tokens,
        top_p,
        frequency_penalty,
        presence_penalty,
        stream: true,
      }, { responseType: 'stream' });   //https://platform.openai.com/docs/api-reference/chat/create#chat-create-response_format


      //gpt-4	$0.03 / 1K tokens	$0.06 / 1K tokens
      //gpt-4-32k	$0.06 / 1K tokens	$0.12 / 1K tokens
      //gpt-4-1106-preview	$0.01 / 1K tokens	$0.03 / 1K tokens
      //gpt-4-1106-vision-preview	$0.01 / 1K tokens	$0.03 / 1K tokens

      let buffer = ''; // Puffer zum Sammeln der Daten

      res.data.on('data', data => {
        buffer += data.toString(); // Daten zum Puffer hinzufügen

        if (buffer.includes('}]}\n')) {
          const lines = buffer.split('\n').filter(line => line.trim() !== '');
          for (const line of lines) {
            const message = line.replace(/^data: /, '');
            if (message === '[DONE]') {
                ws.send(message);
                return; // Stream finished
            }
            try {
              console.log(message)
                const parsed = JSON.parse(message);
                const response = parsed.choices[0].delta
                if (response.content) {
                  ws.send(response.content);
                }
            } catch(error) {
                console.error('Could not JSON parse stream message', message, error);
            }
          }
          buffer = '';
        }
      });
      res.data.on('end', () => {
        return
      });
    } 
    catch (error) {
      console.error(error);
      ws.send(JSON.stringify({ error: error }));
    } 
  });
});




/**
 * GET /
 * Returns a simple message.
 */
app.get('/', (req, res) => {
  res.status(200).send({
    message: "Hello World - I'm going to kill all humans!",
  })
})

/**
 * POST /dalle
 * Returns a response from OpenAI's image generation model.
 */
app.post('/dalle', async (req, res) => {
  const prompt = req.body.prompt
  console.log(prompt)
  try {
    const response = await openai.createImage({
      prompt: `${prompt}`,
      n: 1,
      size: "1024x1024",
      model: "dall-e-3",
      quality: "standard"  
    })

 
   // DALL·E 3	Standard	1024×1024	$0.040 / image
   // Standard	1024×1792, 1792×1024	$0.080 / image

   // DALL·E 3	HD	1024×1024	$0.080 / image
   // HD	1024×1792, 1792×1024	$0.120 / image

   console.log(response.data.data[0].url)
   res.status(200).send({ bot: response.data.data[0].url })
  } catch (error) {
   
    console.error(error.response.data.error) // Log error and return a generic error message
    res.status(500).send({ bot: error.response.data.error.message })
  }
})



// Start server
const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Server listening on port ${port}`))
