# GPT && DALL-E


## install

### client
```bash
cd frontend && npm i
```
### server
```bash
cd server && npm i
```

## Configuration
### Server
1. obtain your openai api key from [here](https://openai.com)
2. `cd server`
3. copy `.env.example` to `.env`
4. add your openai api key to `.env`

## run
### to run client and server concurrently
```bash
cd frontend
npm run start
```